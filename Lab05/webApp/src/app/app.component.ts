import { Component } from '@angular/core';
import {CartiService} from './carti.service';
import 'rxjs/add/operator/map'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private cartiService: CartiService) {
  }

  titlu: string;
  autori: string;
  an: number;
  editura: string;
  cuvinte_cheie: string;
  result: string;

  add() {
    console.log(this.titlu);
    console.log(this.autori);
    this.cartiService.add({titlu: this.titlu, autori: this.autori, an: this.an, 
      editura: this.editura, cuvinte_cheie: this.cuvinte_cheie})
      .map((res) => res.json())
      .subscribe(
        (data) => this.result = data.message,
        (err) => {
          console.log(err);this.result = err.message;});
  }
}
