
import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class CartiService{
    private cartiUrl = 'http://localhost:8080/add';  // URL to web api
    private headers = new Headers({'Content-Type': 'application/json'});

    constructor(private http: Http) { }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }   

    add(data) {
        return this.http.post(this.cartiUrl, JSON.stringify(data), {headers: this.headers});
    }
}