var express  = require('express');
var app      = express();             
const path = require('path');
var bodyParser = require('body-parser');
var server = require('http').createServer(app);
let fs = require("fs");

app.use(express.static(__dirname + '/public'));                 
app.use(bodyParser.urlencoded({'extended':'true'}));            
app.use(bodyParser.json());                                     

app.use(express.static(path.join(__dirname, 'dist')));

app.route('/add').post((req, res) => {
  var error = '';
  var carte = req.body;
  console.log(carte);
  if(!typeof carte.titlu) error += 'Titlu invalid;';
  var autori = carte.autori.split(',');
  var regAut = new RegExp('[A-Za-z ]+');
  var autoriValid = true;
  autori.forEach(element => {
    if(!regAut.test(element)) {
      autoriValid = false;
    }
  });
  if(!autoriValid) error += 'Autor invalid;';
  if(autori.lenght === 0) error += "Nu exista autori;";
  var regAn = new RegExp('[1-9][0-9]*');
  if(Number(carte.an)<0 || Number(carte.an)>(new Date()).getFullYear() || !regAn.test(carte.an)) {
    error += 'An invalid; ';
  }
  if(!typeof carte.editura) error += 'Editura invalida;';
  if(carte.cuvinte_cheie.split(',').lenght === 0)  error += 'Cuvinte invalide;';
  if(error !== '') res.send({success: false, message: error});
  else {
    var stream = fs.createWriteStream(path.join(__dirname, 'src/assets/data.txt'), {flags:'a'});
    stream.write(JSON.stringify(req.body) + "\n", (err) => {
      if (err) res.send({success: false, message: 'Eroare: nu s-a putut salva'});
      res.send({success: true, message: 'Cartea a fost adaugata'});
    });
  }
});

server.listen(8080);
console.log("Server started: 8080");

