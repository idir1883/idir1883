package idir1883MV.repository.repoMock;

import idir1883MV.model.Carte;
import idir1883MV.repository.repoInterfaces.CartiRepositoryInterface;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class CartiRepositoryMockTest_TC09 {

    private CartiRepositoryInterface cartiRepository;

    @Before
    public void setUp() throws Exception {
        cartiRepository = new CartiRepositoryMock();
    }

    @After
    public void tearDown() throws Exception {
        cartiRepository = null;
    }

    @Test
    public void cautaCarteDupaAutor_TC09() {
        List<Carte> expectedResult = new ArrayList();
        expectedResult = cartiRepository.getCarti();
        assertArrayEquals(expectedResult.toArray(), cartiRepository.cautaCarteDupaAutor("i").toArray());
    }
}