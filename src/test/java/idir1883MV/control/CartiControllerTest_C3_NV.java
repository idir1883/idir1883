package idir1883MV.control;

import idir1883MV.model.Carte;
import idir1883MV.repository.repoInterfaces.CartiRepositoryInterface;
import idir1883MV.repository.repoMock.CartiRepositoryMock;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertArrayEquals;

public class CartiControllerTest_C3_NV {

    private CartiRepositoryInterface cartiRepository;
    private CartiController cartiController;

    @Before
    public void setUp() throws Exception {
        cartiRepository = new CartiRepositoryMock();
        cartiController = new CartiController(cartiRepository);
    }

    @After
    public void tearDown() throws Exception {
        cartiRepository = null;
        cartiController = null;
    }

    @Test
    public void C3_1() throws Exception {
        int year = 2100;
        try {
            cartiController.getCartiOrdonateDinAnul(year);
        } catch (Exception e) {
            assertArrayEquals("An invalid!".toCharArray(), e.getMessage().toCharArray());
            return;
        }
        assertFalse("Test failed - invalid year", true);
    }
}
