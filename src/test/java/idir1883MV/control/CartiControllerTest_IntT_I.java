package idir1883MV.control;

import idir1883MV.model.Carte;
import idir1883MV.repository.repoMock.*;
import idir1883MV.view.Consola;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.*;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;

public class CartiControllerTest_IntT_I {

    CartiControllerTest_TC1_ECP_V ecp1V;
    CartiControllerTest_TC6_ECP_NV ecp6NV;
    CartiControllerTest_TC5_ECP_NV ecp5NV;
    CartiControllerTest_TC4_BVA_V bva4V;
    CartiControllerTest_TC5_BVA_NV bva5NV;
    CartiControllerTest_TC6_BVA_V bva6V;
    CartiRepositoryMockTest_TC05 tc05;
    CartiRepositoryMockTest_TC06 tc06;
    CartiRepositoryMockTest_TC07 tc07;
    CartiRepositoryMockTest_TC08 tc08;
    CartiRepositoryMockTest_TC09 tc09;

    CartiControllerTest_C3_V c3V;
    CartiControllerTest_C3_NV c3NV;

    CartiRepositoryMock cartiRepositoryMock;
    CartiController cartiController;
    Consola consola;

    @Before
    public void setUp( ) throws Exception
    {
        ecp1V = new CartiControllerTest_TC1_ECP_V();
        ecp6NV= new CartiControllerTest_TC6_ECP_NV();
        ecp5NV = new CartiControllerTest_TC5_ECP_NV();
        bva4V = new CartiControllerTest_TC4_BVA_V();
        bva5NV = new CartiControllerTest_TC5_BVA_NV();
        bva6V = new CartiControllerTest_TC6_BVA_V();
        tc05 = new CartiRepositoryMockTest_TC05();
        tc06 = new CartiRepositoryMockTest_TC06();
        tc07 = new CartiRepositoryMockTest_TC07();
        tc08 = new CartiRepositoryMockTest_TC08();
        tc09 = new CartiRepositoryMockTest_TC09();

        c3V = new CartiControllerTest_C3_V();
        c3NV = new CartiControllerTest_C3_NV();
        cartiRepositoryMock = new CartiRepositoryMock();
        cartiController = new CartiController( cartiRepositoryMock );
        consola = new Consola( cartiController );
    }

    @After
    public void tearDown( )
    { }

    @org.junit.Test()
    public void test()
    {
        testareA();
        testareB();
        testareC();
        testIntP();
    }


    public void testIntP() {
        testIntA();
        testIntB();
        testIntC();
    }

    public void testareA( )
    {
        //testare A
        try
        {
            ecp1V.setUp();
            ecp1V.adaugaCarte_TC1_ECP();
            ecp1V.tearDown();

            ecp6NV.setUp();
            ecp6NV.adaugaCarte_TC6_ECP();
            ecp6NV.tearDown();

            ecp5NV.setUp();
            ecp5NV.adaugaCarte_TC5_ECP();
            ecp5NV.tearDown();

            bva4V.setUp();
            bva4V.adaugaCarte_TC4_BVA();
            bva4V.tearDown();

            bva5NV.setUp();
            bva5NV.adaugaCarte_TC5_BVA();
            bva5NV.tearDown();

            bva6V.setUp();
            bva6V.adaugaCarte_TC6_BVA();
            bva6V.tearDown();

            assertTrue( true );
        }
        catch ( Exception e )
        {
            assertFalse( "Error",true );
        }

    }

    public void testareB( )
    {
        //testare B
        try
        {
            tc05.setUp();
            tc05.cautaCarteDupaAutor_TC05();
            tc05.tearDown();

            tc06.setUp();
            tc06.cautaCarteDupaAutor_TC06();
            tc06.tearDown();

            tc07.setUp();
            tc07.cautaCarteDupaAutor_TC07();
            tc07.tearDown();

            tc08.setUp();
            tc08.cautaCarteDupaAutor_TC08();
            tc08.tearDown();

            tc09.setUp();
            tc09.cautaCarteDupaAutor_TC09();
            tc09.tearDown();
            assertTrue( true );
        }
        catch ( Exception e )
        {
            assertFalse( "Error",true );
        }

    }

    public void testareC( )
    {
        //testare C
        try
        {
            c3V.setUp();
            c3V.C3_1();
            c3V.tearDown();

            c3NV.setUp();
            c3NV.C3_1();
            c3NV.tearDown();

            assertTrue( true );
        }
        catch ( Exception e )
        {
            assertFalse( "Error",true );
        }
    }

    public void testIntA() {
        try
        {
            System.setIn( new FileInputStream( "in1.txt" ) );
            System.setOut( new PrintStream( "out.txt" ) );
            consola.executa();
            Carte c = Carte.getCarteFromString("Poezii;Eminescu;2000;Corint;luceafar;raza");
            boolean found = cartiRepositoryMock.cautaCarte( c );
            assertTrue( found == true );
        }
        catch ( IOException e )
        {
            assertFalse( true );
        }
    }

    public void testIntB() {
        try
        {
            System.setIn( new FileInputStream( "in2.txt" ) );
            System.setOut( new PrintStream( "out.txt" ) );
            consola.executa();
            BufferedReader reader = new BufferedReader( new FileReader( "out.txt" ) );
            StringBuilder contentBuilder = new StringBuilder(  );
            String content = "";
            reader.lines().forEach( line -> contentBuilder.append( line + ";" ));

            content = contentBuilder.toString();
            assertTrue( content.contains( "Test;Calinescu,Tetica;1992;Pipa;am,casa;" ));
        }
        catch ( IOException e )
        {
            assertFalse( true );
        }
    }

    public void testIntC() {
        try
        {
            System.setIn( new FileInputStream( "in3.txt" ) );
            System.setOut( new PrintStream( "out.txt" ) );
            consola.executa();

            BufferedReader reader = new BufferedReader( new FileReader( "out.txt" ) );
            StringBuilder contentBuilder = new StringBuilder(  );
            String content = "";
            reader.lines().forEach( line -> contentBuilder.append( line + ";"));

            content = contentBuilder.toString();
            assertTrue( content.contains( "Test;Calinescu,Tetica;1992;Pipa;am,casa" ));
        }
        catch ( IOException e )
        {
            assertFalse(true);
        }
    }
}
