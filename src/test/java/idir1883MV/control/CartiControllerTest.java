package idir1883MV.control;

import idir1883MV.model.Carte;
import idir1883MV.repository.repoInterfaces.CartiRepositoryInterface;
import idir1883MV.repository.repoMock.CartiRepositoryMock;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

public class CartiControllerTest {

    private Carte carte1;
    private CartiController cartiController;
    private CartiRepositoryInterface cartiRepository;

    @Before
    public void setUp() throws Exception {
        carte1 = new Carte();
        carte1.setTitlu("Moara cu noroc");
        carte1.setAutori(Arrays.asList("Ioan Slavici"));
        carte1.setAnAparitie(2010);
        carte1.setEditura("Humanitas");
        carte1.setCuvinteCheie(Arrays.asList("moara","noroc"));
        cartiRepository = new CartiRepositoryMock();
        cartiController = new CartiController(cartiRepository);
    }

    @After
    public void tearDown() throws Exception {
        carte1 = null;
        cartiRepository = null;
        cartiController = null;
    }

    @Test
    public void adaugaCarte_TC1_ECP() throws Exception {
        assert(cartiController.getCarti().size() == 6);
        cartiController.adaugaCarte(carte1);
        assert(cartiController.getCarti().size() == 7);
    }

    @Test
    public void adaugaCarte_TC5_ECP() {
        carte1.setTitlu("Povestiri");
        carte1.setAutori(Arrays.asList("Mihai Eminescu", "Ion Creanga2"));
        carte1.setAnAparitie(2010);
        carte1.setEditura("Humanitas");
        carte1.setCuvinteCheie(Arrays.asList("povestiri"));
        try {
            cartiController.adaugaCarte(carte1);
        } catch (Exception e) {
            assertArrayEquals("Autor invalid!".toCharArray(), e.getMessage().toCharArray());
            return;
        }
        assertFalse("Test failed - invalid input data", true);
    }

    @Test
    public void adaugaCarte_TC6_ECP() {
        carte1.setTitlu("Povestiri");
        carte1.setAutori(Arrays.asList("Mihai Eminescu", "Ion Creanga"));
        carte1.setAnAparitie(-2010);
        carte1.setEditura("Humanitas");
        carte1.setCuvinteCheie(Arrays.asList("povestiri"));
        try {
            cartiController.adaugaCarte(carte1);
        } catch (Exception e) {
            assertArrayEquals("An invalid!".toCharArray(), e.getMessage().toCharArray());
            return;
        }
        assertFalse("Test failed - invalid input data", true);
    }

    @Test
    public void adaugaCarte_TC4_BVA() throws Exception {
        carte1.setTitlu("Ion");
        carte1.setAutori(Arrays.asList("Liviu Rebreanu"));
        carte1.setAnAparitie(2017);
        carte1.setEditura("Nemira");
        carte1.setCuvinteCheie(Arrays.asList("pamant"));
        assert(cartiController.getCarti().size() == 6);
        cartiController.adaugaCarte(carte1);
        assert(cartiController.getCarti().size() == 7);
    }

    @Test
    public void adaugaCarte_TC5_BVA() {
        carte1.setTitlu("Ion");
        carte1.setAutori(Arrays.asList("Liviu Rebreanu"));
        carte1.setAnAparitie(2019);
        carte1.setEditura("Nemira");
        carte1.setCuvinteCheie(Arrays.asList("pamant"));
        try {
            cartiController.adaugaCarte(carte1);
        } catch (Exception e) {
            assertArrayEquals("An invalid!".toCharArray(), e.getMessage().toCharArray());
            return;
        }
        assertFalse("Test failed - invalid input data", true);
    }

    @Test
    public void adaugaCarte_TC6_BVA() throws Exception {
        carte1.setTitlu("Ion");
        carte1.setAutori(Arrays.asList("Liviu Rebreanu"));
        carte1.setAnAparitie(2018);
        carte1.setEditura("Nemira");
        carte1.setCuvinteCheie(Arrays.asList("pamant"));
        assert(cartiController.getCarti().size() == 6);
        cartiController.adaugaCarte(carte1);
        assert(cartiController.getCarti().size() == 7);
    }
}