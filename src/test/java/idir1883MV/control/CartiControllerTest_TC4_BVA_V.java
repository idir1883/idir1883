package idir1883MV.control;

import idir1883MV.model.Carte;
import idir1883MV.repository.repoInterfaces.CartiRepositoryInterface;
import idir1883MV.repository.repoMock.CartiRepositoryMock;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.Arrays;

public class CartiControllerTest_TC4_BVA_V {

    private Carte carte1;
    private CartiController cartiController;
    private CartiRepositoryInterface cartiRepository;

    @Before
    public void setUp() throws Exception {
        carte1 = new Carte();
        carte1.setTitlu("Moara cu noroc");
        carte1.setAutori(Arrays.asList("Ioan Slavici"));
        carte1.setAnAparitie(2010);
        carte1.setEditura("Humanitas");
        carte1.setCuvinteCheie(Arrays.asList("moara","noroc"));
        cartiRepository = new CartiRepositoryMock();
        cartiController = new CartiController(cartiRepository);
    }

    @After
    public void tearDown() throws Exception {
        carte1 = null;
        cartiRepository = null;
    }


    @Test
    public void adaugaCarte_TC4_BVA() throws Exception {
        carte1.setTitlu("Ion");
        carte1.setAutori(Arrays.asList("Liviu Rebreanu"));
        carte1.setAnAparitie(2017);
        carte1.setEditura("Nemira");
        carte1.setCuvinteCheie(Arrays.asList("pamant"));
        assert(cartiController.getCarti().size() == 6);
        cartiController.adaugaCarte(carte1);
        assert(cartiController.getCarti().size() == 7);
    }

}
