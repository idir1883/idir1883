package idir1883MV.control;

import idir1883MV.model.Carte;
import idir1883MV.repository.repoInterfaces.CartiRepositoryInterface;
import idir1883MV.repository.repoMock.CartiRepositoryMock;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertArrayEquals;

public class CartiControllerTest_C3_V {

    private CartiRepositoryInterface cartiRepository;
    private CartiController cartiController;

    @Before
    public void setUp() throws Exception {
        cartiRepository = new CartiRepositoryMock();
        cartiController = new CartiController(cartiRepository);
    }

    @After
    public void tearDown() throws Exception {
        cartiRepository = null;
        cartiController = null;
    }

    @Test
    public void C3_1() throws Exception {
        List<Carte> expectedResult = new ArrayList<Carte>();
        cartiController.getCartiOrdonateDinAnul(1992);
        expectedResult.add(Carte.getCarteFromString("Test;Calinescu,Tetica;1992;Pipa;am,casa"));
        assertArrayEquals(expectedResult.toArray(), cartiRepository.getCartiOrdonateDinAnul(1992).toArray());
    }
}
