package idir1883MV.control;

import idir1883MV.model.Carte;
import idir1883MV.repository.repoInterfaces.CartiRepositoryInterface;
import idir1883MV.repository.repoMock.CartiRepositoryMock;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.Arrays;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertFalse;

public class CartiControllerTest_TC6_ECP_NV {

    private Carte carte1;
    private CartiRepositoryInterface cartiRepository;
    private CartiController cartiController;

    @Before
    public void setUp() throws Exception {
        carte1 = new Carte();
        carte1.setTitlu("Moara cu noroc");
        carte1.setAutori(Arrays.asList("Ioan Slavici"));
        carte1.setAnAparitie(2010);
        carte1.setEditura("Humanitas");
        carte1.setCuvinteCheie(Arrays.asList("moara","noroc"));
        cartiRepository = new CartiRepositoryMock();
        cartiController = new CartiController(cartiRepository);
    }

    @After
    public void tearDown() throws Exception {
        carte1 = null;
        cartiRepository = null;
    }

    @Test
    public void adaugaCarte_TC6_ECP() {
        carte1.setTitlu("Povestiri");
        carte1.setAutori(Arrays.asList("Mihai Eminescu", "Ion Creanga"));
        carte1.setAnAparitie(-2010);
        carte1.setEditura("Humanitas");
        carte1.setCuvinteCheie(Arrays.asList("povestiri"));
        try {
            cartiController.adaugaCarte(carte1);
        } catch (Exception e) {
            assertArrayEquals("An invalid!".toCharArray(), e.getMessage().toCharArray());
            return;
        }
        assertFalse("Test failed - invalid input data", true);
    }

}
