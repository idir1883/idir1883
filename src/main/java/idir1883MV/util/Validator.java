package idir1883MV.util;

import idir1883MV.model.Carte;

import java.util.Calendar;

public class Validator {
	
	public static void isStringOK(String s) throws Exception{
		boolean flag = s.matches("[a-zA-Z]+");
		if(flag == false)
			throw new Exception("String invalid");
	}
	
	public static void validateCarte(Carte c)throws Exception{
		if(c.getCuvinteCheie()==null){
			throw new Exception("Lista cuvinte cheie vida!");
		}
		if(c.getAutori()==null){
			throw new Exception("Lista autori vida!");
		}
		if(!isTitleValid(c.getTitlu()))
			throw new Exception("Titlu invalid!");
		if(!isStringValid(c.getEditura()))
			throw new Exception("Editura invalida!");
		for(String s:c.getAutori()){
			if(!isStringValid(s))
				throw new Exception("Autor invalid!");
		}
		for(String s:c.getCuvinteCheie()){
			if(!isStringValid(s))
				throw new Exception("Cuvant cheie invalid!");
		}
		if(!Validator.idValidYear(c.getAnAparitie()))
			throw new Exception("An invalid!");
	}
	
	public static boolean idValidYear(int s) {
		return s>=0 && s<=Calendar.getInstance().get(Calendar.YEAR);
	}
	
	public static boolean isStringValid(String s){
		String []t = s.split(" ");
		if(t.length>1){
			for(int i=0;i<t.length;i++) {
				if(!t[i].matches("[a-zA-Z]+")){
					return false;
				}
			}
			return true;
		}
		return s.matches("[a-zA-Z]+");
	}

	public static boolean isTitleValid(String s){
		String []t = s.split(" ");
		for(int i=0;i<t.length;i++) {
			if(!t[i].matches("[a-zA-Z]+|[0-9]+")){
				return false;
			}
		}
		return true;
	}


}
