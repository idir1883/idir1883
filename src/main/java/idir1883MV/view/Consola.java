package idir1883MV.view;


import idir1883MV.control.CartiController;
import idir1883MV.control.CartiController;
import idir1883MV.model.Carte;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Consola {

	private BufferedReader console;
	CartiController cartiController;
	
	public Consola(CartiController bc){
		this.cartiController=bc;
	}
	
	public void executa() throws IOException {
		
		console = new BufferedReader(new InputStreamReader(System.in));
		
		int opt = -1;
		while(opt!=0){
			
			switch(opt){
				case 1:
					adauga();
					break;
				case 2:
					modifica();
					break;
				case 3:
					sterge();
					break;
				case 4:
					cautaCartiDupaAutor();
					break;
				case 5:
					cautaCartiDupaCuvantCheie();
					break;
				case 6:
					afiseazaCartiOrdonateDinAnul();
					break;
				case 7:
					printCarti();
					break;
			}
		
			printMenu();
			String line;
			do{
				System.out.println("Introduceti un nr:");
				line=console.readLine();
			}while(!line.matches("[0-9]+"));
			opt=Integer.parseInt(line);
		}
	}
	
	public void printMenu(){
		System.out.println("\n\n\n");
		System.out.println("Evidenta cartilor dintr-o biblioteca");
		System.out.println("     1. Adaugarea unei noi carti");
		System.out.println("     2. Modificarea unei carti");
		System.out.println("     3. Stergerea unei carti");
		System.out.println("     4. Cautarea cartilor scrise de un anumit autor");
		System.out.println("     5. Cautarea cartilor dupa un cuvant cheie");
		System.out.println("     6. Afisarea cartilor din biblioteca care au aparut intr-un anumit an, ordonate alfabetic dupa titlu si autori");
		System.out.println("     7. Vizualizarea tutor cartilor");
		System.out.println("     0. Exit");
	}

	private Carte citesteCarte() {
		Carte c = new Carte();
		try {
			System.out.println("\n\n\n");

			System.out.println("Titlu:");
			c.setTitlu(console.readLine());

			String line;
			do {
				System.out.println("Nr. de autori:");
				line = console.readLine();
			} while (!line.matches("[1-9]+"));
			int nrAutori = Integer.parseInt(line);
			for (int i = 1; i <= nrAutori; i++) {
				System.out.println("Autor " + i + ": ");
				c.adaugaReferent(console.readLine());
			}

			System.out.println("An aparitie:");
			line = console.readLine();
			c.setAnAparitie(Integer.parseInt((line)));

			System.out.println("Editura:");
			c.setEditura(console.readLine());


			do {
				System.out.println("Nr. de cuvinte cheie:");
				line = console.readLine();
			} while (!line.matches("[1-9]|10"));
			int nrCuv = Integer.parseInt(line);
			for (int i = 1; i <= nrCuv; i++) {
				System.out.println("Cuvant " + i + ": ");
				c.adaugaCuvantCheie(console.readLine());
			}
		} catch (Exception e) {
            System.out.println(e.getMessage());
		}
		return c;
	}


	public void adauga() {
		Carte c = citesteCarte();
		try {
			cartiController.adaugaCarte(c);
            System.out.println("Cartea a fost adaugata!");
		} catch (Exception e) {
            System.out.println(e.getMessage());
		}
	}

	public void sterge() {
		Carte c = citesteCarte();
		try {
			cartiController.stergeCarte(c);
            System.out.println("Cartea a fost stearsa!");
		} catch (Exception e) {
            System.out.println(e.getMessage());
		}
	}

	public void modifica() {
		Carte c = citesteCarte();
		System.out.println("Carte noua: ");
		try {
            Carte noua = citesteCarte();
			cartiController.modificaCarte(c, noua);
            System.out.println("Cartea a fost modificata!");
		} catch (Exception e) {
            System.out.println(e.getMessage());
		}

	}


	public void cautaCartiDupaAutor(){
		System.out.println("\n\n\n");
		System.out.println("Autor:");

		try {
			String autor = console.readLine();
			for(Carte carte: cartiController.cautaCarteDupaAutor(autor)){
				System.out.println(carte);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
            System.out.println(e.getMessage());
		} catch (Exception e) {
			// TODO Auto-generated catch block
            System.out.println(e.getMessage());
		}
	}

	public void cautaCartiDupaCuvantCheie(){
		System.out.println("\n\n\n");
		System.out.println("Cuvant:");

		try {
			String cuvant = console.readLine();
			for(Carte carte: cartiController.cautaCarteDupaCuvantCheie(cuvant)){
				System.out.println(carte);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
            System.out.println(e.getMessage());
		} catch (Exception e) {
			// TODO Auto-generated catch block
            System.out.println(e.getMessage());
		}
	}

	public void printCarti() {
		for(Carte c: cartiController.getCarti()) {
			System.out.println(c.toString());
		}
	}

	public void afiseazaCartiOrdonateDinAnul(){
		System.out.println("\n\n\n");
		try{
			String line;
			System.out.println("An aparitie:");
			line=console.readLine();
			int an = Integer.parseInt(line);
			for(Carte c: cartiController.getCartiOrdonateDinAnul(an)){
				System.out.println(c);
			}
		}catch(Exception e){
            System.out.println(e.getMessage());
		}
		
	}

}
