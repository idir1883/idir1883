package idir1883MV.control;


import idir1883MV.model.Carte;
import idir1883MV.repository.repoInterfaces.CartiRepositoryInterface;
import idir1883MV.util.Validator;

import java.util.List;

public class CartiController {

	private CartiRepositoryInterface cr;

	public CartiController(CartiRepositoryInterface cr){
		this.cr = cr;
	}

	public void adaugaCarte(Carte c) throws Exception{
		Validator.validateCarte(c);
		cr.adaugaCarte(c);
	}

	public void modificaCarte(Carte veche, Carte noua) throws Exception{
		Validator.validateCarte(veche);
		Validator.validateCarte(noua);
		cr.modificaCarte(veche, noua);
	}

	public List<Carte> getCarti() {
		return cr.getCarti();
	}

	public void stergeCarte(Carte c) throws Exception{
		cr.stergeCarte(c);
	}

	public List<Carte> cautaCarteDupaAutor(String autor) throws Exception{
		if(!Validator.isStringValid(autor))
			throw new Exception("Autor invalid!");
		return cr.cautaCarteDupaAutor(autor);
	}

	public List<Carte> cautaCarteDupaCuvantCheie(String cuvant) throws Exception{
		Validator.isStringOK(cuvant);
		return cr.cautaCarteDupaCuvantCheie(cuvant);
	}

	public List<Carte> getCartiOrdonateDinAnul(int an) throws Exception{
		if(!Validator.idValidYear(an))
			throw new Exception("An invalid!");
		return cr.getCartiOrdonateDinAnul(an);
	}

	
}
