package idir1883MV.model;


import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Carte {
	
	private String titlu;
	private List<String> autori;
	private int anAparitie;
	private String editura;
	private List<String> cuvinteCheie;
	
	public Carte(){
		titlu = "";
		autori = new ArrayList<String>();
		anAparitie = 0;
		editura = "";
		cuvinteCheie = new ArrayList<String>();
	}

	public String getTitlu() {
		return titlu;
	}

	public void setTitlu(String titlu) {
		this.titlu = titlu;
	}

	public int getAnAparitie() {
		return anAparitie;
	}

	public void setAnAparitie(int anAparitie) {
		this.anAparitie = anAparitie;
	}

	public List<String> getCuvinteCheie() {
		return cuvinteCheie;
	}

	public void setCuvinteCheie(List<String> cuvinteCheie) {
		this.cuvinteCheie = cuvinteCheie;
	}

	public List<String> getAutori() { return autori; }

	public String getEditura() { return editura; }

	public void setAutori(List<String> autori) { this.autori = autori; }

	public void setEditura(String editura) { this.editura = editura; }
	
	public void adaugaCuvantCheie(String cuvant){
		cuvinteCheie.add(cuvant);
	}
	
	public void adaugaReferent(String ref){
		autori.add(ref);
	}

	public boolean cautaDupaAutor(String autor){
		for(String a: autori){
			if(a.contains(autor))
				return true;
		}
		return false;
	}

	@Override
	public String toString(){
		String ref = "";
		String cuvCheie = "";
		
		for(int i=0;i<autori.size();i++){
			if(i==autori.size()-1)
				ref+=autori.get(i);
			else
				ref+=autori.get(i)+",";
		}
		
		for(int i=0;i<cuvinteCheie.size();i++){
			if(i==cuvinteCheie.size()-1)
				cuvCheie+=cuvinteCheie.get(i);
			else
				cuvCheie+=cuvinteCheie.get(i)+",";
		}
		
		return titlu+";"+ref+";"+anAparitie+";"+editura+";"+cuvCheie;
	}
	
	public static Carte getCarteFromString(String carte){
		Carte c = new Carte();
		String []atr = carte.split(";");

		c.titlu=atr[0];

		String []autori = atr[1].split(",");
		for(String s:autori){
			c.adaugaReferent(s);
		}
		c.anAparitie = Integer.parseInt(atr[2]);
		c.editura=atr[3];

		String []cuvCheie = atr[4].split(",");


		for(String s:cuvCheie){
			c.adaugaCuvantCheie(s);
		}
		
		return c;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Carte carte = (Carte) o;
		return anAparitie == carte.anAparitie &&
				((Carte) o).getTitlu().equals(carte.titlu) &&
				((Carte) o).getAutori().equals(carte.autori) &&
				((Carte) o).getEditura().equals(carte.editura) &&
				((Carte) o).getCuvinteCheie().equals(carte.cuvinteCheie);
	}
}
