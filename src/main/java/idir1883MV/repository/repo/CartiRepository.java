package idir1883MV.repository.repo;


import idir1883MV.model.Carte;
import idir1883MV.repository.repoInterfaces.CartiRepositoryInterface;

import java.io.*;
import java.util.*;

public class CartiRepository implements CartiRepositoryInterface {
	
	private String file = "cartiBD.dat";
	
	public CartiRepository(){
	}
	
	@Override
	public void adaugaCarte(Carte c) {
		BufferedWriter bw = null;
		try {
			bw = new BufferedWriter(new FileWriter(file,true));
			bw.write(c.toString());
			bw.newLine();
			
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void writeAllToFile(List<Carte> carti) {
		BufferedWriter bw = null;
		try {
			bw = new BufferedWriter(new FileWriter(file));
			for(Carte c:carti) {
				bw.write(c.toString());
				bw.newLine();
			}
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<Carte> getCarti() {
		List<Carte> lc = new ArrayList<Carte>();
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(file));
			String line = null;
			while((line=br.readLine())!=null){
				lc.add(Carte.getCarteFromString(line));
			}
			
			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return lc;
	}

	public boolean cautaCarte(Carte carte) {
		List<Carte> carti = getCarti();
		for(Carte c: carti) {
			if(c.equals(carte)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public void modificaCarte(Carte veche, Carte noua) throws Exception {
		// TODO Auto-generated method stub
		List<Carte> carti = getCarti();
		boolean found = false;
		for(Carte c: carti) {
			if(c.equals(veche)) {
				c.setTitlu(noua.getTitlu());
				c.setAutori(noua.getAutori());
				c.setEditura(noua.getEditura());
				c.setAnAparitie(noua.getAnAparitie());
				c.setCuvinteCheie(noua.getCuvinteCheie());
				found = true;
			}
		}
		System.out.println(found);
		if(found == false) {
			System.out.println("not found!");
			throw new Exception("Nu exista cartea introdusa!");
		}
		writeAllToFile(carti);
	}

	@Override
	public void stergeCarte(Carte c) {
		// TODO Auto-generated method stub
		List<Carte> carti = getCarti();
		for(Carte carte: carti) {
			if(carte.equals(c)) {
				carti.remove(carte);
			}
		}
		writeAllToFile(carti);
	}

    @Override
    public List<Carte> cautaCarteDupaCuvantCheie(String cuvant) {
		List<Carte> carti = getCarti();
		List<Carte> cartiGasite = new ArrayList<Carte>();
		int i=0;
		while (i<carti.size()){
			List<String> lref = carti.get(i).getCuvinteCheie();
			int j = 0;
			while(j<lref.size()){
				if(lref.get(j).toLowerCase().contains(cuvant.toLowerCase())){
					cartiGasite.add(carti.get(i));
					break;
				}
				j++;
			}
			i++;
		}
		return cartiGasite;
    }

    @Override
	public List<Carte> cautaCarteDupaAutor(String autor) {
		List<Carte> carti = getCarti();
		List<Carte> cartiGasite = new ArrayList<Carte>();
		int i = 0;
		while (i < carti.size()){
			if (carti.get(i).cautaDupaAutor(autor)) {
				cartiGasite.add(carti.get(i));
			}
			i++;
		}
		return cartiGasite;
	}

	@Override
	public List<Carte> getCartiOrdonateDinAnul(int an) {
		List<Carte> lc = getCarti();
		List<Carte> lca = new ArrayList<Carte>();
		for(Carte c:lc){
			if(c.getAnAparitie() == an){
				lca.add(c);
			}
		}

		Collections.sort(lca,new Comparator<Carte>(){

			@Override
			public int compare(Carte a, Carte b) {
				if(a.getTitlu().toLowerCase().compareTo(b.getTitlu().toLowerCase())==0){
					int i=0;
					while(i<a.getAutori().size() && i<b.getAutori().size() && a.getAutori().get(i).equals(b.getAutori().get(i))){
						i++;
					}
					return a.getAutori().get(i).toLowerCase().compareTo(b.getAutori().get(i).toLowerCase());
				}
				return a.getTitlu().toLowerCase().compareTo(b.getTitlu().toLowerCase());
			}

		});
		
		return lca;
	}

}
