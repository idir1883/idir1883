package idir1883MV.repository.repoInterfaces;


import idir1883MV.model.Carte;

import java.util.List;

public interface CartiRepositoryInterface {
	void adaugaCarte(Carte c);
	void modificaCarte(Carte nou, Carte vechi) throws Exception;
	void stergeCarte(Carte c) throws Exception;
	List<Carte> cautaCarteDupaAutor(String autor);
    List<Carte> cautaCarteDupaCuvantCheie(String cuvant);
	List<Carte> getCarti();
	List<Carte> getCartiOrdonateDinAnul(int an);
}
